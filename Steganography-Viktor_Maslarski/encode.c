#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "encode.h"
#include "types.h"
#include "common.h"

// Extern required values from main file
extern uint raster_data, passcode_len, step;
extern uchar magic_string_signature[CHAR_SIZE + MAX_PASSCODE_LEN + CHAR_SIZE];

/* Function Definitions */
/*Validation part-1 started*/
/* 1. Check operation type */
OperationType check_operation_type(char *argv[])
{
	if (!strcmp(*argv, "-e")) // Check if argument is '-e'
		return e_encode;
	else if (!strcmp(*argv, "-d")) // Check if argument is '-d'
		return e_decode;
	else
		return e_unsupported; // Some other argument
}

//---------------------------------------------------
// DESCRIPTION: 
//				- If the argument is "-e", it returns e_encode enum value, indicating that the program should perform encoding.
//              - If the argument is "-d", it returns e_decode enum value, indicating that the program should perform decoding.
//              - If the argument is not recognized, it returns e_unsupported enum value, indicating that the operation type is not supported.
// FUNCTION: 
//				This function checks the operation type requested by the user based on the command line arguments passed to the program.
//
//
//---------------------------------------------------

/* 2. Read and validate Encode args from argv */
Status read_and_validate_bmp_format(char *argv[])
{
	// Pointer to hold address of '.bmp' part from given argument
	const char *bmp_holder = strstr(*argv, ".bmp");
	if (bmp_holder) // Error handling
	{				// If '.bmp' part is found, then check if string exactly ends with '.bmp'
		return (!strcmp(bmp_holder, ".bmp")) ? e_success : e_failure;
	}
	return e_failure; // NULL address which means '.bmp' part is not found
}

//---------------------------------------------------
// DESCRIPTION:
//				- It first searches for the ".bmp" substring within the first argument of argv using the strstr() function, which returns a pointer to the first occurrence of the substring in the argument.
//				- If the substring is found, it checks if the substring is located at the end of the argument by comparing the substring's memory address to ".bmp" using the strcmp() function.
//				- If the comparison returns zero, it means that the argument ends with ".bmp" and the function returns e_success.
//				- If the argument does not end with ".bmp", the function returns e_failure.
//				- If the ".bmp" substring is not found in the argument, the function also returns e_failure.
// FUNCTION: 
//				Read and validate Encode args from argv
//
//
//---------------------------------------------------


/* ------------------------3------------------------ */
Status read_and_validate_extn(uchar_ptr sec_file_name_holder, EncodeInfo *encInfo)
{
	// Pointer to hold the heap memory of the size of filename including '\0' character
	uchar_ptr sec = (uchar_ptr)malloc(strlen((const char *)sec_file_name_holder) + 1);
	if (sec == NULL)
	{
		printf("ERROR: Unable to allocate dynamic memory.\n\n");
		return e_failure;
	}
	strcpy((char *)sec, (const char *)sec_file_name_holder); // Store the filename inside allocated heap
	uint secret_filename_len = strlen((const char *)sec);	 // Get length of filename
	char *ext = strtok((char *)sec, ".");					 // Get part of string before dot
	// If there is no dot in the filename, length of string remains the same
	if (strlen(ext) == secret_filename_len)
	{
		printf("ERROR: There is no dot in the given filename.\n");
		return e_failure;
	}
	// Extract the extension of secret file (i.e. part of string after dot)
	ext = strtok(NULL, ".");
	strcpy((char *)encInfo->extn_secret_file, (const char *)ext); // Store the extracted extension
	// Get and store length of secret extension
	encInfo->secret_extn_len = strlen((const char *)encInfo->extn_secret_file);
	// Validate extension size
	if (encInfo->secret_extn_len > MAX_FILE_SUFFIX)
	{
		printf("ERROR: file extension should not exceed 4 characters\n");
		return e_failure;
	}
	free(sec);		  // Free the allocated block of memory
	return e_success; // No errors found
}

//---------------------------------------------------
// DESCRIPTION:
//				- The function first allocates memory dynamically to store the sec_file_name_holder parameter, which holds the secret file name.
//				- It then copies the secret file name from the sec_file_name_holder parameter to the allocated heap memory using the strcpy() function.
//				- The function then extracts the file extension by splitting the string at the dot "." using the strtok() function. The first part of the string before the dot is discarded.
//				- The extracted file extension is stored in the encInfo->extn_secret_file field of the encInfo structure using the strcpy() function.
//				- The function then calculates the length of the file extension and stores it in the encInfo->secret_extn_len field of the encInfo structure.
//				- It then checks if the length of the file extension is greater than the maximum allowed file extension size defined by MAX_FILE_SUFFIX and returns e_failure if it is.
//				- Finally, the function frees the allocated heap memory using the free() function and returns e_success if no errors are encountered.
// FUNCTION:
//				Read, validate and extract secret file extension
//
//---------------------------------------------------
 

/* ------------------------4------------------------ */
Status no_digits(const char *str)
{
	while (*str != '\0') // Iterate till end of string
	{
		if (*str < '0' || *str > '9') // If any non-digit character found
			return e_success;
		str++;
	}
	return e_failure; // All characters are digits
}

//---------------------------------------------------
// DESCRIPTION:
//				- The function iterates through each character in the input string using a while loop.
//				- For each character, it checks if the character is less than the character '0' or greater than the character '9', which are the ASCII values for the digits 0 to 9.
//				- If the character is not a digit, the function immediately returns e_success, indicating that there are non-digit characters in the string.
//				- If the function iterates through the entire string without finding any non-digit characters, it returns e_failure, indicating that all characters in the string are digits.
// FUNCTION:
//				Function to check non-digit character in passcode
//
//---------------------------------------------------

/* ------------------------5------------------------ */
Status open_files(EncodeInfo *encInfo)
{
	// Open source image file with proper error handling
	if ((encInfo->fptr_src_image = fopen((const char *)encInfo->src_image_fname, "rb")) == NULL)
	{
		perror("fopen");
		fprintf(stderr, "ERROR: Unable to open file %s. This file may not be present in the current project directory.\n\n", encInfo->src_image_fname);
		return e_failure;
	}
	// Open secret file with proper error handling
	if ((encInfo->fptr_secret = fopen((const char *)encInfo->secret_fname, "rb")) == NULL)
	{
		perror("fopen");
		fprintf(stderr, "ERROR: Unable to open file %s. This file may not be present in the current project directory.\n\n", encInfo->secret_fname);
		return e_failure;
	}
	// Open output image file with proper error handling
	if ((encInfo->fptr_stego_image = fopen((const char *)encInfo->stego_image_fname, "wb")) == NULL)
	{
		perror("fopen");
		fprintf(stderr, "ERROR: Unable to open file %s. This file may not be present in the current project directory.\n\n", encInfo->stego_image_fname);
		return e_failure;
	}
	return e_success; // No error found
}
/*Validation part-1 completed*/

//---------------------------------------------------
// DESCRIPTION:
//				- The function first opens the source image file for reading using the fopen() function. It checks if the file is opened successfully using a NULL check.
//				- If the file fails to open, the function prints an error message using perror() and fprintf() functions and returns e_failure.
//				- The function then opens the secret file for reading using the fopen() function. It checks if the file is opened successfully using a NULL check.
//				- If the file fails to open, the function prints an error message using perror() and fprintf() functions and returns e_failure.
//				- The function then opens the stego image file for writing using the fopen() function. It checks if the file is opened successfully using a NULL check.
//				- If the file fails to open, the function prints an error message using perror() and fprintf() functions and returns e_failure.
//				- If all files are opened successfully, the function returns e_success.
// FUNCTION:
//				Get File pointers for input and output files
//
//---------------------------------------------------


/*Validation part-2 started*/
/* ------------------------6------------------------ */
Status copy_bmp_header(FILE *fptr_src_image, FILE *fptr_dest_image)
{
	// Store image header of the size of raster data into a block of heap memory
	uchar_ptr img_header = (uchar_ptr)malloc(raster_data * sizeof(uchar));
	if (img_header == NULL) // Error handling for malloc
	{
		printf("ERROR: Unable to allocate dynamic memory.\n\n");
		exit(e_success);
	}
	// Read bytes of the size of raster data from source file
	fread(img_header, raster_data, 1, fptr_src_image);
	if (ferror(fptr_src_image)) // Error handling while reading from file
	{
		printf("ERROR: Error in reading source image file.\n\n");
		return e_failure;
	}
	// Write data obtained in heap onto the destination file
	fwrite(img_header, raster_data, 1, fptr_dest_image);
	if (ferror(fptr_dest_image)) // Error handling while writing onto destination file
	{
		printf("ERROR: Error in writing onto destination image file.\n\n");
		return e_failure;
	}
	free(img_header); // Free dynamically allocated block of memory
	return e_success; // No error found
}

//---------------------------------------------------
// DESCRIPTION:
//				- The function copy_bmp_header takes two file pointers as input arguments - fptr_src_image and fptr_dest_image. 
//				- The fptr_src_image is a pointer to the source BMP image file from which the BMP header data is to be read, 
//				and fptr_dest_image is a pointer to the output BMP image file to which the BMP header data is to be written.
//				- The function first calculates the size of the BMP header data by multiplying the height, width, 
//				and bits per pixel of the image, and then allocates a block of heap memory of this size using the malloc function.
//				- Then, the function reads BMP header data from the source file using the fread function, and writes it to the output file using the fwrite function. 
//				- It also performs error handling using the ferror function to detect any errors that occur during file reading or writing.
//				- It frees the dynamically allocated block of memory using the free function and returns the status of the operation.
// FUNCTION:
//				Copy bmp image header
//
//---------------------------------------------------

/* ------------------------7------------------------ */
uint get_image_size_for_bmp(FILE *fptr_image)
{
	uint img_size; // Return value to be read from image file
	// Seek to 34th byte to get image data size from the '.bmp' image file
	fseek(fptr_image, 34L, SEEK_SET);
	// Read the width (an unsigned integer)
	fread(&img_size, sizeof(img_size), 1, fptr_image);
	if (ferror(fptr_image))
	{
		printf("ERROR: Error while reading from the image file.\n\n");
		exit(e_success);
	}
	// Return image capacity
	return img_size;
}

//---------------------------------------------------
// DESCRIPTION:
//				- This function reads the size of an image from a BMP file by seeking to the 34th byte,
//				where the image data size is stored, and then reading an unsigned integer (4 bytes) representing the size.
//				- It returns the size of the image as an unsigned integer.
//				- If there is an error while reading from the file, the function prints an error message and exits the program with a success status.
// FUNCTION:
//				Get image size
//
//---------------------------------------------------

/* ------------------------8------------------------ */
uint get_file_size(FILE *fptr) // Returns file size including EOF byte
{
	// Seek to the end of file
	fseek(fptr, 0L, SEEK_END);
	return (uint)ftell(fptr); // Return file index value
}

//---------------------------------------------------
// DESCRIPTION:
//				- This function returns the size of a file, including the EOF (End of File) byte, by seeking to the end of the file using fseek with the SEEK_END parameter. 
//				- The current position indicator is set to the end of the file, 
//				and then the ftell function is called to get the current position indicator's value, which is the file size.
//				- The file size is cast to an unsigned integer and returned as the function's result.
// FUNCTION:
//				Gets file size
//
//---------------------------------------------------

/* ------------------------9------------------------ */
Status check_capacity(EncodeInfo *encInfo)
{
	// Print messages
	printf("INFO.%d: Image data size = %u bytes\n", ++step, encInfo->image_capacity);
	printf("INFO.%d: Magic string size = %u bytes\n", ++step, encInfo->magic_string_size);
	// Check if image data size is greater than magic string size
	return (encInfo->magic_string_size < encInfo->image_capacity) ? e_success : e_failure;
}
/*Validation part-2 completed*/

//---------------------------------------------------
// DESCRIPTION:
//				- The function checks if the image data size, which is stored in the encInfo structure pointed to by encInfo->image_capacity,
//				is greater than the magic string size, which is stored in the same structure as encInfo->magic_string_size.
//				- The function first prints the image data size and magic string size as information messages to the console,
//				along with a step number, which is incremented for each message.
//				- The function then returns e_success if the image data size is greater than the magic string size,
//				indicating that the capacity is sufficient for encoding, and e_failure otherwise, indicating that the capacity is not sufficient.
//				- The return value of the function can be used by the calling program to check whether encoding can proceed or not.
// FUNCTION:
//				Checks capacity
//
//---------------------------------------------------


/*Encoding part started*/
/* ------------------------10------------------------ */
Status do_encoding(EncodeInfo *encInfo)
{
	// Encode magic string signature
	printf("INFO.%d: Encoding Magic String Signature\n", ++step);
	fseek(encInfo->fptr_src_image, raster_data, SEEK_SET);
	if (encode_magic_string((const char *)magic_string_signature, encInfo))
	{
		printf("INFO.%d: Magic string signature successfully encoded.\n", ++step);
	}
	else
	{
		printf("ERROR: Error while encoding magic string signature.\n\n");
		return e_failure;
	}
	// Check if passcode is given
	if (passcode_len)
	{
		// Encode passcode length
		printf("INFO.%d: Encoding passcode length\n", ++step);
		if (encode_int_size_expression(passcode_len, encInfo))
		{
			printf("INFO.%d: Passcode length successfully encoded.\n", ++step);
		}
		else
		{
			printf("ERROR: Error while encoding passcode length.\n\n");
			return e_failure;
		}
		// Encode passcode
		printf("INFO.%d: Encoding the passcode\n", ++step);
		if (encode_magic_string((const char *)encInfo->passcode, encInfo))
		{
			printf("INFO.%d: Successfully encoded the passcode.\n", ++step);
		}
		else
		{
			printf("ERROR: Error while encoding the passcode.\n\n");
			return e_failure;
		}
	}
	// Encode secret file extension length
	printf("INFO.%d: Encoding secret file extension length\n", ++step);
	if (encode_int_size_expression(encInfo->secret_extn_len, encInfo))
	{
		printf("INFO.%d: Secret file extension length successfully encoded.\n", ++step);
	}
	else
	{
		printf("ERROR: Error while encoding secret file extension length.\n\n");
		return e_failure;
	}
	// Encode the dot in secret file name
	printf("INFO.%d: Encoding the dot in secret file name\n", ++step);
	if (encode_magic_string(".", encInfo))
	{
		printf("INFO.%d: Successfully encoded the dot.\n", ++step);
	}
	else
	{
		printf("ERROR: Error while encoding the dot.\n\n");
		return e_failure;
	}
	// Encode the secret file extension
	printf("INFO.%d: Encoding the secret file extension\n", ++step);
	if (encode_magic_string((const char *)(encInfo->extn_secret_file), encInfo))
	{
		printf("INFO.%d: Successfully encoded the secret file extension.\n", ++step);
	}
	else
	{
		printf("ERROR: Error while encoding the secret file extension.\n\n");
		return e_failure;
	}
	// Encode the secret file size
	printf("INFO.%d: Encoding the secret file size\n", ++step);
	if (encode_int_size_expression(encInfo->size_secret_file - CHAR_SIZE, encInfo))
	{
		printf("INFO.%d: Secret file size successfully encoded.\n", ++step);
	}
	else
	{
		printf("ERROR: Error while encoding secret file size.\n\n");
		return e_failure;
	}
	// Encode the secret data
	// Let's create a string to store the secret_data
	uchar_ptr secret_data = (uchar_ptr)malloc(encInfo->size_secret_file * sizeof(uchar));
	if (secret_data == NULL)
	{
		printf("ERROR: Unable to allocate dynamic memory.\n\n");
		return e_failure;
	}
	rewind(encInfo->fptr_secret);
	fread(secret_data, encInfo->size_secret_file * sizeof(uchar) - CHAR_SIZE, 1, encInfo->fptr_secret);
	secret_data[encInfo->size_secret_file - CHAR_SIZE] = '\0'; // Set last character as NUL character
	// Now let's encode secret data
	printf("INFO.%d: Encoding the secret data\n", ++step);
	if (encode_magic_string((const char *)secret_data, encInfo))
	{
		printf("INFO.%d: Successfully encoded the secret data.\n", ++step);
	}
	else
	{
		printf("ERROR: Error while encoding the secret data.\n\n");
		return e_failure;
	}
	free(secret_data);
	// Copy remaining image bytes
	printf("INFO.%d: Copying the left over data\n", ++step);
	if (copy_remaining_image_data((FILE *)encInfo->fptr_src_image, (FILE *)encInfo->fptr_stego_image, encInfo->image_capacity - encInfo->magic_string_size + CHAR_SIZE)) // To append EOF i.e. the last byte, we should add byte size
	{
		printf("INFO.%d: Remaining image data copied to output file successfully.\n", ++step);
	}
	else
	{
		printf("ERROR: Failed to copy remaining image data\n\n");
		return e_failure;
	}
	return e_success; // No error found
}

//---------------------------------------------------
// DESCRIPTION:
//				- This function encodes the secret message into the cover image using the given encoding algorithm.
//				- It encodes the magic string signature, passcode (if provided), secret file extension length, secret file extension,
// 				secret file size, and secret data into the cover image in a specific order.
//				- It prints the progress and success/error messages to the console. 
//				- It also copies the remaining bytes of the cover image after encoding the secret message into the output stego image file.
//				- Finally, it returns the status of the encoding process as e_success if it completes successfully, and e_failure if any errors occur during the encoding process.
// FUNCTION:
//				Perform encoding
//
//---------------------------------------------------

/* ------------------------11------------------------ */
Status encode_magic_string(const char *magic_string, EncodeInfo *encInfo)
{
	uchar scan_char; // Read and store each byte into a character
	// Outer iteration till the size of given string
	for (uint i = 0; i < strlen(magic_string); i++)
	{
		for (int j = 7; j >= 0; j--) // 8 times inner iteration, Note that iterator j should not be of the type uint.
		{							 // Read each byte
			fread(&scan_char, sizeof(scan_char), 1, encInfo->fptr_src_image);
			if (ferror(encInfo->fptr_src_image)) // Error handling
			{
				printf("ERROR: Error while reading from source image file.\n\n");
				return e_failure;
			}
			scan_char &= 0xFE;				 // Clear the least significant bit of fetched character
			if (magic_string[i] & (01 << j)) // Check every bit of magic string
			{
				scan_char |= 01; // Set the least significant bit of obtained character
			}
			else
			{
				scan_char |= 00; // Clear the least significant bit of obtained character
			}
			// Write the obtained byte into output file
			fwrite(&scan_char, sizeof(scan_char), 1, encInfo->fptr_stego_image);
			if (ferror(encInfo->fptr_stego_image)) // Error handling
			{
				printf("ERROR: Error while writing onto output image file.\n\n");
				return e_failure;
			}
		}
	}
	return e_success; // No errors found
}

//---------------------------------------------------
// DESCRIPTION:
//				This function encodes a given magic string into the least significant bit of each byte of the cover image using the LSB (Least Significant Bit) technique.
//				The function takes a pointer to the magic string and a pointer to the EncodeInfo structure as inputs. It reads each byte of the cover image and clears its least significant bit.
//				It then checks each bit of the magic string, and if the bit is 1, sets the least significant bit of the byte to 1, otherwise, it clears the least significant bit of the byte.
//				After encoding each bit of the magic string into the least significant bit of each byte of the cover image, it writes the obtained byte into the output stego image file. 
//				The function returns e_success if the encoding process completes successfully without any errors, and e_failure if any errors occur during the encoding process.
// FUNCTION:
//				Encode Magic String
//
//---------------------------------------------------

/* ------------------------12------------------------ */
Status encode_int_size_expression(uint len, EncodeInfo *encInfo)
{
	uchar scan_char;							// Read and store each byte into a character
	for (int j = INT_SIZE * 8 - 1; j >= 0; j--) // Fetch every byte till integer size
	{
		// Read each byte
		fread(&scan_char, sizeof(scan_char), 1, encInfo->fptr_src_image);
		if (ferror(encInfo->fptr_src_image)) // Error handling
		{
			printf("ERROR: Error while reading from source image file.\n\n");
			return e_failure;
		}
		scan_char &= 0xFE;	// Clear the least significant bit of obtained character
		if (len & (1 << j)) // Check every bit of obtained length
		{
			scan_char |= 01; // Set the least significant bit of obtained character
		}
		else
		{
			scan_char |= 00; // Clear the least significant bit of obtained character
		}
		// Write obtained byte onto output file
		fwrite(&scan_char, sizeof(scan_char), 1, encInfo->fptr_stego_image);
		if (ferror(encInfo->fptr_stego_image)) // Error handling
		{
			printf("ERROR: Error while writing into output image file.\n\n");
			return e_failure;
		}
	}
	return e_success; // No errors found
}

//---------------------------------------------------
// DESCRIPTION:
//				- The function uses a loop to fetch every byte of the integer size (4 bytes) from the source image file in reverse order.
//				- For each byte, the function clears the least significant bit of the obtained character using a bitwise AND operation with 0xFE.
//				- The function then checks every bit of the given integer length using a bitwise AND operation with (1 << j) where j is the index of the bit starting from the most significant bit of the integer.
//				- If the bit is set, the function sets the least significant bit of the obtained character to 1 using a bitwise OR operation with 01, otherwise, it clears the least significant bit of the obtained character using a bitwise OR operation with 00.
//				- The obtained byte is then written onto the output stego image file using the fwrite() function.
//				- The function performs error handling by checking if there is any error while reading from the source image file or writing into the output image file.
//				- If all bytes are encoded successfully, the function returns e_success. Otherwise, it returns e_failure.
// FUNCTION:
//				Encode secret file extenstion
//
//---------------------------------------------------

/* ------------------------13------------------------ */
Status copy_remaining_image_data(FILE *fptr_src_image, FILE *fptr_dest_image, uint f_size)
{
	// Pointer to hold heap memory of the size of file
	uchar_ptr ch = (uchar_ptr)malloc(f_size * sizeof(uchar));
	if (ch == NULL)
	{
		printf("ERROR: Unable to allocate dynamic memory.\n\n");
		return e_failure;
	}
	fread(ch, f_size, 1, fptr_src_image); // Read and store all the data of file size
	if (ferror(fptr_src_image))			  // Error handling
	{
		printf("ERROR: Error in reading from source file.\n\n");
		return e_failure;
	}
	fwrite(ch, f_size, 1, fptr_dest_image); // Write the obtained data onto output file
	if (ferror(fptr_dest_image))			// Error handling
	{
		printf("ERROR: Error in writing onto output file.\n\n");
		return e_failure;
	}
	free(ch); // Free allocated heap memory
	return e_success;
}

//---------------------------------------------------
// DESCRIPTION:
//				- This function is used to copy the remaining data from the source image file to the destination image file after embedding the secret data.
//				- The function first allocates heap memory of size f_size using malloc.
//				- It then reads all the remaining data from the source image file into the allocated heap memory using fread. 
//				- If any error occurs while reading the file, it returns e_failure.
//				- The function writes the obtained data from the heap memory onto the destination image file using fwrite. 
//				- If any error occurs while writing onto the file, it returns e_failure. 
//				- Finally, it frees the heap memory allocated using free and returns e_success if the operation completes successfully
// FUNCTION:
//				Copy remaining image bytes from src_image to dest_image after encoding
//
//---------------------------------------------------


/*End of encoding part*/
